output "db_instance_address" {
  value       = "${aws_db_instance.default.address}"
  description = "The hostname of the RDS instance. See also endpoint and port."
}

output "db_instance_arn" {
  value       = "${aws_db_instance.default.arn}"
  description = "The ARN of the RDS instance."
}

output "db_instance_endpoint" {
  value       = "${aws_db_instance.default.endpoint}"
  description = ".The connection endpoint in address:port format."
}

output "db_instance_engine" {
  value       = "${aws_db_instance.default.engine}"
  description = "The database engine."
}

output "db_instance_engine_version" {
  value       = "${aws_db_instance.default.engine_version}"
  description = "The database engine version."
}

output "db_instance_id" {
  value       = "${aws_db_instance.default.id}"
  description = "The RDS instance ID."
}

output "db_instance_name" {
  value       = "${aws_db_instance.default.name}"
  description = "The database name."
}

output "db_instance_status" {
  value       = "${aws_db_instance.default.status}"
  description = "The RDS instance status."
}
