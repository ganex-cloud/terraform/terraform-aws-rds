module "rds-NAME" {
  source            = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-rds?ref=master"
  engine            = "mysql"
  port              = "3306"
  identifier        = "NAME"
  engine_version    = "5.7.22"
  instance_class    = "db.t2.micro"
  allocated_storage = 20
  username          = "root"
  password          = "${file("secrets/rds-NAME-password.txt")}"
  vpc_id            = "vpc-xxxxxxxxxx"

  subnet_ids = [
    "subnet-xxxxxxxxxxxxxxxxx",
    "subnet-xxxxxxxxxxxxxxxxx",
    "subnet-xxxxxxxxxxxxxxxxx",
  ]

  source_security_group_id = [
    "sg-xxxxxxxxxxxxxxxxx",
    "sg-xxxxxxxxxxxxxxxxx",
  ]

  maintenance_window        = "mon:06:00-mon:06:30"
  backup_window             = "04:30-05:00"
  apply_immediately         = false
  major_engine_version      = "5.7"
  final_snapshot_identifier = "NAME-final-snapshot"
}
