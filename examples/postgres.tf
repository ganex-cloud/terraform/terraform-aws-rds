module "rds-NAME" {
  source            = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-rds?ref=master"
  engine            = "postgres"
  port              = "5432"
  identifier        = "NAME"
  engine_version    = "11.1"
  instance_class    = "db.t2.small"
  allocated_storage = 20
  username          = "postgres"
  password          = "${file("secrets/rds-NAME-password.txt")}"
  vpc_id            = "vpc-xxxx"

  subnet_ids = [
    "subnet-xxxxxxxx",
    "subnet-xxxxxxxx",
    "subnet-xxxxxxxx",
  ]

  source_security_group_id = [
    "sg-xxxxxxxx",
  ]

  maintenance_window        = "mon:06:00-mon:07:00"
  backup_window             = "04:10-05:10"
  apply_immediately         = false
  major_engine_version      = "11"
  final_snapshot_identifier = "NAME-final-snapshot"
}
